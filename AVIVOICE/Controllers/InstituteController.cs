﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using BOL;
using System.Data;

namespace AVIVOICE.Controllers
{
    public class InstituteController : Controller
    {
        //
        // GET: /Institute/
        int returnvf = 0;
        public ActionResult InstituteDetails()
        {
            return View();
        }

        public JsonResult InstituteDetail(institute ins)
        {
            try
            {
                if (ins.ID >= 1)
                {
                    returnvf = InstituteLogic.GetInstance.UpdateInstitute(ins);
                }
                else
                {
                    returnvf = InstituteLogic.GetInstance.InsertInstitute(ins);
                }

                if (returnvf > 1) { return Json("Records added Successfully."); }
                else if (returnvf == -1)
                {
                    return Json("Records Already Exists.");
                }
                else if (returnvf == -2)
                {
                    return Json("Failed to add Record.");
                }
                else { return Json(""); }
            }
            catch (Exception ex) {
                return Json("Records not added : " + ex.ToString());
            }
        }

        //SELECTADMIN
        public JsonResult SelectAdmin()
        {
            DataTable dt = InstituteLogic.GetInstance.SelectAdmin();
            try
            {

                List<institute> AdminList = new List<institute>();
                AdminList = (from DataRow dr in dt.Rows
                             select new institute()
                             {
                                 ID = Convert.ToInt32(dr["ID"]),
                                 OwnerName = dr["OwnerName"].ToString(),
                                 InsType = dr["InsType"].ToString(),
                                 InsAdd = dr["InsAdd"].ToString(),
                                 Mobile = dr["Mobile"].ToString(),
                                 Email = dr["Email"].ToString(),
                                 Pass = dr["Pass"].ToString(),

                             }).ToList();

                return Json(AdminList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                return Json("could not find any Admin" + Ex.ToString());
            }
        }

        //GETBYID
        public JsonResult getbyID(int id)
        {
            DataTable dt = InstituteLogic.GetInstance.getbyID(id);
            // MasterState EmpRepo = new MasterState();
            List<institute> AdminList = new List<institute>();
            AdminList = (from DataRow dr in dt.Rows
                         select new institute()
                         {
                             ID = Convert.ToInt32(dr["ID"]),
                             OwnerName = dr["OwnerName"].ToString(),
                             InsType = dr["InsType"].ToString(),
                             InsAdd = dr["InsAdd"].ToString(),
                             Mobile = dr["Mobile"].ToString(),
                             Email = dr["Email"].ToString(),
                             // Pass = dr["Pass"].ToString(),
                         }).ToList();
            return Json(AdminList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int ID)
        {
            try
            {
                InstituteLogic.GetInstance.Delete(ID);
                return Json("Deleted Successfully");
            }
            catch (Exception ex)
            {
                return Json("couldn't delete" + ex.ToString());
            }

        }
	}
}