﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AVIVOICE.Startup))]
namespace AVIVOICE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
