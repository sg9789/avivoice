﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOL;
using DAL;
using System.Data;

namespace BLL
{
    public class InstituteLogic
    {
        #region Code to make this as Singleton Class
        /// <summary>
        /// Private static member to implement Singleton Desing Pattern
        /// </summary>
        private static InstituteLogic instance = new InstituteLogic();

        /// <summary>
        /// Static property of the class which will provide the singleton instance of it
        /// </summary>
        public static InstituteLogic GetInstance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        public int InsertInstitute(institute theRole)
        {
            return DAL.InstituteDataAccess.GetInstance.InsertInstitute(theRole);
        }

        public int UpdateInstitute(institute theRole)
        {
            return DAL.InstituteDataAccess.GetInstance.UpdateInstitute(theRole);
        }

        public DataTable SelectAdmin()
        {
            return DAL.InstituteDataAccess.GetInstance.SelectAdmin();
        }

        public DataTable getbyID(int id)
        {
            return DAL.InstituteDataAccess.GetInstance.getbyID(id);
        }
        public void Delete(int id)
        {
            DAL.InstituteDataAccess.GetInstance.Delete(id);
        }


    }
}
