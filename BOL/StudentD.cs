﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class StudentD
    {
        public int SID { get; set; }

        public String Pass { get; set; }

        public String Name { get; set; }

        public String FatherName { get; set; }

        public String MotherName { get; set; }

        public String Sex { get; set; }

        public DateTime DOB { get; set; }

        public String Category { get; set; }

        public String Nationality { get; set; }

        public String State { get; set; }

        public long ContactNo { get; set; }

        public String Email { get; set; }

        public String Occupation { get; set; }

        public String OccupationPlace { get; set; }

        public String PresentAdd { get; set; }

        public String PermanentAdd { get; set; }

        public int CID { get; set; }

        public String SOI { get; set; }

        public Byte TOC { get; set; }
    }
}
