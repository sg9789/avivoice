﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class FeeDetail
    {
        public int FID { get; set; }

        public int SID { get; set; }

        public Decimal FPaid { get; set; }

        public DateTime PaidDate { get; set; }

        public Decimal FDue { get; set; }

        public DateTime DueDate { get; set; }

        public String PayMode { get; set; }
    }
}
