﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
    public class StudentDetail
    {
        public int CID { get; set; }

        public String CName { get; set; }

        public String CDesc { get; set; }

        public Decimal CFees { get; set; }

        public int CDur { get; set; }
    }
}
