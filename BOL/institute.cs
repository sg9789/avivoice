﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOL
{
   public class institute
    {

        public int ID { get; set; }

        public String OwnerName { get; set; }

        public String InsType { get; set; }

        public String InsAdd { get; set; }

        public String Mobile { get; set; }

        public String Email { get; set; }

        public String Pass { get; set; }
    }
}
