﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;

namespace BOL
{
    public partial class Common
    {
        public static string GetSTRXMLResult(DataTable dtTable)
        {
            string strXMLResult = "";
            if (dtTable != null)
            {
                if (dtTable.Rows.Count > 0)
                {
                    StringWriter sw = new StringWriter();
                    dtTable.TableName = "MyTable";
                    dtTable.WriteXml(sw);
                    strXMLResult = sw.ToString();
                    sw.Close();
                    sw.Dispose();
                }
            }
            return strXMLResult;
        }

        public static string ReturnZeroIfNull(string val)
        {
            string ReturnValue = string.Empty;

            if (string.IsNullOrEmpty(val))
            {
                ReturnValue = "0";
            }
            else
            {
                ReturnValue = val;
            }

            return ReturnValue;
        }

        public static List<institute> LoggedInUser
        {
            get
            {
                var myfileAttachmentList = new List<institute>();
                myfileAttachmentList = (List<institute>)HttpContext.Current.Session["LoggedInUser"];
                return myfileAttachmentList;
            }
            set
            {

                HttpContext.Current.Session.Add("LoggedInUser", value);
            }
        }
    }
}