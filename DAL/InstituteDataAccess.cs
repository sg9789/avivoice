﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOL;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;

namespace DAL
{
    public partial class InstituteDataAccess : SqlDataAccessFile
    {
        #region Code to make this as Singleton Class
        /// <summary>
        /// Private static member to implement Singleton Desing Pattern
        /// </summary>
        private static InstituteDataAccess instance = new InstituteDataAccess();

        /// <summary>
        /// Static property of the class which will provide the singleton instance of it
        /// </summary>
        public static InstituteDataAccess GetInstance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Methods & Implementation



        public int InsertInstitute(institute theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand InsertCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "InstituteInsert" })
            {
                InsertCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                InsertCommand.Parameters.Add(GetParameter("@OwnerName", SqlDbType.VarChar, theRole.OwnerName));
                InsertCommand.Parameters.Add(GetParameter("@InsType", SqlDbType.VarChar, theRole.InsType));
                InsertCommand.Parameters.Add(GetParameter("@InsAdd", SqlDbType.VarChar, theRole.InsAdd));
                InsertCommand.Parameters.Add(GetParameter("@Mobile", SqlDbType.VarChar, theRole.Mobile));
                InsertCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                InsertCommand.Parameters.Add(GetParameter("@AddBy", SqlDbType.Int, 1));


                ExecuteStoredProcedure(InsertCommand);

                ReturnValue = int.Parse(InsertCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

        public int UpdateInstitute(institute theRole)
        {
            int ReturnValue = 0;
            using (SqlCommand UpdateCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "InstituteUpdate" })
            {
                UpdateCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                UpdateCommand.Parameters.Add(GetParameter("@OwnerName", SqlDbType.VarChar, theRole.OwnerName));
                UpdateCommand.Parameters.Add(GetParameter("@ID", SqlDbType.Int, theRole.ID));
                UpdateCommand.Parameters.Add(GetParameter("@InsType", SqlDbType.VarChar, theRole.InsType));
                UpdateCommand.Parameters.Add(GetParameter("@InsAdd", SqlDbType.VarChar, theRole.InsAdd));
                UpdateCommand.Parameters.Add(GetParameter("@Mobile", SqlDbType.VarChar, theRole.Mobile));
                UpdateCommand.Parameters.Add(GetParameter("@Email", SqlDbType.VarChar, theRole.Email));
                //UpdateCommand.Parameters.Add(GetParameter("@ModBy", SqlDbType.Int, Common.LoggedInUser[0].ID));
                UpdateCommand.Parameters.Add(GetParameter("@ModBy", SqlDbType.Int, 1));


                ExecuteStoredProcedure(UpdateCommand);

                ReturnValue = int.Parse(UpdateCommand.Parameters[0].Value.ToString());

                return ReturnValue;
            }
        }

        public DataTable SelectAdmin()
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "SelectAdmin" })
            {

                return ExecuteGetDataTable(SelectCommand);
            }
        }

        public DataTable getbyID(int id)
        {
            using (SqlCommand SelectCommand = new System.Data.SqlClient.SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "SelectAdminById" })
            {
                //SelectCommand.Parameters.Add(GetParameter("@ReturnValue", SqlDbType.Int, ReturnValue)).Direction = ParameterDirection.Output;
                SelectCommand.Parameters.Add(GetParameter("@ID", SqlDbType.Int, id));
                return ExecuteGetDataTable(SelectCommand);
            }
        }

        public void Delete(int ID)
        {
            using (SqlCommand DeleteCommand = new SqlCommand { CommandType = CommandType.StoredProcedure, CommandText = "InstituteDelete" })
            {
                DeleteCommand.Parameters.Add(GetParameter("@ID", SqlDbType.Int, ID));

                ExecuteGetDataTable(DeleteCommand);

            }

        }
    }
}
        #endregion